package com.company;

public class Main {

    public static void main(String[] args) {
        String strMainOne = "One one one one. Two two two. Yes Yes.";
        String strMainTwo;
        String strMainThree;
        int strLengthOne, strLengthTwo;

        strLengthOne = strMainOne.length();
        strMainTwo = strMainOne.replace(" ", "");
        strLengthTwo = strMainTwo.length();
        System.out.println("В строке " + (strLengthOne - strLengthTwo + 1) + " слов");
        strLengthOne = 0;
        strLengthTwo = 0;

        strLengthOne = strMainOne.length();
        strMainThree = strMainOne.replace(".", "");
        strLengthTwo = strMainThree.length();
        System.out.println("В строке " + (strLengthOne - strLengthTwo) + " предложений");
    }
}
